vit@vit-vm-ubuntu:~/task3.23$ cat Dockerfile 
FROM alpine:latest
ARG MYARG
RUN apk update && apk add build-base


vit@vit-vm-ubuntu:~/task3.23$ docker build . -t cache:1
[+] Building 15.6s (6/6) FINISHED                                                               
 => [internal] load build definition from Dockerfile                                       0.0s
 => => transferring dockerfile: 103B                                                       0.0s
 => [internal] load .dockerignore                                                          0.0s
 => => transferring context: 2B                                                            0.0s
 => [internal] load metadata for docker.io/library/alpine:latest                           0.0s
 => [1/2] FROM docker.io/library/alpine:latest                                             0.0s
 => [2/2] RUN apk update && apk add build-base                                            14.7s
 => exporting to image                                                                     0.9s
 => => exporting layers                                                                    0.9s
 => => writing image sha256:db5a4ada698c8bf6b8a481cde35eec5d585033dba0113164ac82d132d1027  0.0s 
 => => naming to docker.io/library/cache:1                                                 0.0s 
vit@vit-vm-ubuntu:~/task3.23$ docker build . -t cache:2
[+] Building 0.0s (6/6) FINISHED                                                                
 => [internal] load build definition from Dockerfile                                       0.0s
 => => transferring dockerfile: 103B                                                       0.0s
 => [internal] load .dockerignore                                                          0.0s
 => => transferring context: 2B                                                            0.0s
 => [internal] load metadata for docker.io/library/alpine:latest                           0.0s
 => [1/2] FROM docker.io/library/alpine:latest                                             0.0s
 => CACHED [2/2] RUN apk update && apk add build-base                                      0.0s
 => exporting to image                                                                     0.0s
 => => exporting layers                                                                    0.0s
 => => writing image sha256:5cadeb52c9462262f214942fee501da83e99ab619e3b1924a1a608c242b38  0.0s
 => => naming to docker.io/library/cache:2                                                 0.0s
vit@vit-vm-ubuntu:~/task3.23$ docker build . -t cache:2 --no-cache
[+] Building 15.9s (6/6) FINISHED                                                               
 => [internal] load .dockerignore                                                          0.0s
 => => transferring context: 2B                                                            0.0s
 => [internal] load build definition from Dockerfile                                       0.0s
 => => transferring dockerfile: 103B                                                       0.0s
 => [internal] load metadata for docker.io/library/alpine:latest                           0.0s
 => CACHED [1/2] FROM docker.io/library/alpine:latest                                      0.0s
 => [2/2] RUN apk update && apk add build-base                                            15.0s
 => exporting to image                                                                     0.9s 
 => => exporting layers                                                                    0.9s 
 => => writing image sha256:fdf36c83048fa179ddc8e9e589b58d52c2c7887172429215207a34de73986  0.0s 
 => => naming to docker.io/library/cache:2                                                 0.0s 
vit@vit-vm-ubuntu:~/task3.23$ docker build . -t cache:3 --build-arg MYARG=3
[+] Building 15.0s (6/6) FINISHED                                                               
 => [internal] load build definition from Dockerfile                                       0.0s
 => => transferring dockerfile: 103B                                                       0.0s
 => [internal] load .dockerignore                                                          0.0s
 => => transferring context: 2B                                                            0.0s
 => [internal] load metadata for docker.io/library/alpine:latest                           0.0s
 => CACHED [1/2] FROM docker.io/library/alpine:latest                                      0.0s
 => [2/2] RUN apk update && apk add build-base                                            14.1s
 => exporting to image                                                                     0.9s
 => => exporting layers                                                                    0.9s
 => => writing image sha256:445fd813ca2c81eed6ed7cf03221c466a7ab23dfef6a0b593f0529f344d8d  0.0s 
 => => naming to docker.io/library/cache:3                                                 0.0s 
vit@vit-vm-ubuntu:~/task3.23$ docker build . -t cache:3 --build-arg MYARG=3                     
[+] Building 0.0s (6/6) FINISHED                                                                
 => [internal] load build definition from Dockerfile                                       0.0s
 => => transferring dockerfile: 103B                                                       0.0s
 => [internal] load .dockerignore                                                          0.0s
 => => transferring context: 2B                                                            0.0s
 => [internal] load metadata for docker.io/library/alpine:latest                           0.0s
 => [1/2] FROM docker.io/library/alpine:latest                                             0.0s
 => CACHED [2/2] RUN apk update && apk add build-base                                      0.0s
 => exporting to image                                                                     0.0s
 => => exporting layers                                                                    0.0s
 => => writing image sha256:445fd813ca2c81eed6ed7cf03221c466a7ab23dfef6a0b593f0529f344d8d  0.0s
 => => naming to docker.io/library/cache:3                                                 0.0s
vit@vit-vm-ubuntu:~/task3.23$ docker build . -t cache:4 --build-arg MYARG=4
[+] Building 15.4s (6/6) FINISHED                                                               
 => [internal] load build definition from Dockerfile                                       0.0s
 => => transferring dockerfile: 103B                                                       0.0s
 => [internal] load .dockerignore                                                          0.0s
 => => transferring context: 2B                                                            0.0s
 => [internal] load metadata for docker.io/library/alpine:latest                           0.0s
 => CACHED [1/2] FROM docker.io/library/alpine:latest                                      0.0s
 => [2/2] RUN apk update && apk add build-base                                            14.4s
 => exporting to image                                                                     0.9s
 => => exporting layers                                                                    0.9s
 => => writing image sha256:5fe12b862b6adc9d0f927c466459f4232c3cc368211e525f36795332b4a63  0.0s 
 => => naming to docker.io/library/cache:4                                                 0.0s 
vit@vit-vm-ubuntu:~/task3.23$ docker inspect                                                    

